#include <stdio.h>
#include <string.h>
#include <signal.h>
#include "socket.h"
#include <sys/types.h>
#include <sys/wait.h>
int status;

void traitement_signal(int sig) {
	printf("Signal %d reçu\n", sig);
    int pid;
	while((pid = waitpid(-1,&status,WNOHANG))>0){printf("pid terminated %d\n",pid);} //-1 n'importe quel proccessus
}



void initialiser_signaux(void) {
        if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
                perror("signal");
        }
	struct sigaction sa;
	sa.sa_handler = traitement_signal;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	if (sigaction(SIGCHLD, &sa, NULL) == -1) {
		perror("sigaction(SIGCHLD)");
	}
}




int main(int argc, char **argv) {
	/* Arnold Robbins in the LJ of February '95, describing RCS */
	if (argc > 1 && strcmp(argv[1], "-advice") == 0) {
		printf("Don't Panic!\n");
	return 42;
	}
	initialiser_signaux(); 
	printf("Need an advice?\n");
	creer_serveur(8080);
	return 0;
}
