#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include "socket.h"
#include <netinet/in.h>
#include <sys/wait.h>
#include <stdlib.h>
#include "client.h"


int creer_serveur(int port){
//socket
	int socket_serveur;
	socket_serveur = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_serveur == -1) {
		/* traitement de l'erreur */
		perror("socket_serveur");
		exit(1);
	}
	int optval = 1;
	if (setsockopt(socket_serveur, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int)) == -1) {
		perror("Can not set SO_REUSEADDR option");
		exit(1);
	}
	/*Utilisation de la socket serveur */
//bind
	struct sockaddr_in saddr;
	saddr.sin_family = AF_INET; /* Socket ipv4 */
	saddr.sin_port = htons(port); /* Port d'écoute */
	saddr.sin_addr.s_addr = INADDR_ANY; /* écoute sur toutes les interfaces */
	if (bind(socket_serveur, (struct sockaddr *)&saddr, sizeof(saddr)) == -1){
		perror("bind socker_serveur");
		/* traitement de l'erreur */
		exit(1);
	}
//listen
	if (listen(socket_serveur, 10) == -1) {
		perror("listen socket_serveur");
		/* traitement d'erreur */
		exit(1);
	}
//decouper ici ---
	int socket_client;
	while(1){
		socket_client = accept(socket_serveur, NULL, NULL);

		if (socket_client == -1) {
			perror("accept");
			/* traitement d'erreur */
			exit(1);
		}

		int pid = fork();
		if(pid == -1){
			perror("fork fail");
			exit(1);
		}else if(pid > 0) {
			close(socket_client);//  fait une erreur si on le met
		}else{ // à déplacer ailleurs pour une meilleure lisibilité
			 //traitement final
        		traitementClient(socket_client);
			printf("fin du client\n");
			exit(0);
		}
	}
	return 0;
}
